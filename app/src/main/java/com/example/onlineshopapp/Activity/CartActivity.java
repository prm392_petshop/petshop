package com.example.onlineshopapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.example.onlineshopapp.Adapter.CartAdapter;
import com.example.onlineshopapp.Helper.ChangeNumberItemsListener;
import com.example.onlineshopapp.Helper.ManagmentCart;
import com.example.onlineshopapp.R;
import com.example.onlineshopapp.databinding.ActivityCartBinding;

public class CartActivity extends BaseActivity {
    private ActivityCartBinding binding;
    private ManagmentCart managerCart;
    private double tax = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCartBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        managerCart = new ManagmentCart(this);

        setVariable();
        initCartList();
        calculateCart();
    }

    private void calculateCart() {
        double percentTax = 0.02;
        double delivery = 15.0;
        tax = Math.round((managerCart.getTotalFee() * percentTax) * 100) / 100.0;
        double total = Math.round((managerCart.getTotalFee() + tax + delivery) * 100) / 100;
        double itemTotal = Math.round(managerCart.getTotalFee() * 100) / 100;

        binding.totalFeeTxt.setText("$" + itemTotal);
        binding.taxTxt.setText("$" + tax);
        binding.deliveryTxt.setText("$" + delivery);
        binding.totalTxt.setText("$" + total);
    }

    private void initCartList() {
        binding.cartView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        binding.cartView.setAdapter(new CartAdapter(managerCart.getListCart(), this, new ChangeNumberItemsListener() {
            @Override
            public void onChanged() {
                calculateCart();
            }
        }));
    }

    private void setVariable() {
        binding.backBtn.setOnClickListener(v -> finish());
    }
}
