package com.example.onlineshopapp.Activity;

import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;
import com.example.onlineshopapp.Adapter.PicListAdapter;
import com.example.onlineshopapp.Adapter.SizeListAdapter;
import com.example.onlineshopapp.Helper.ManagmentCart;
import com.example.onlineshopapp.Model.ItemsModel;
import com.example.onlineshopapp.R;
import com.example.onlineshopapp.databinding.ActivityDetailBinding;

import java.util.ArrayList;

public class DetailActivity extends BaseActivity {
    private ActivityDetailBinding binding;
    private ItemsModel item;
    private int numberOrder = 1;
    private ManagmentCart managentCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        managentCart = new ManagmentCart(this);

        getBundle();
        initList();
    }

    private void initList() {
        ArrayList<String> sizeList = new ArrayList<>();
        for (String size : item.getSize()) {
            sizeList.add(size);
        }
        binding.sizeList.setAdapter(new SizeListAdapter(sizeList));
        binding.sizeList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        ArrayList<String> colorList = new ArrayList<>();
        for (String imageUrl : item.getPicUrl()) {
            colorList.add(imageUrl);
        }
        Glide.with(this)
                .load(colorList.get(0))
                .into(binding.picMain);
        binding.picList.setAdapter(new PicListAdapter(colorList, binding.picMain));
        binding.picList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    }

    private void getBundle() {
        item = getIntent().getParcelableExtra("object");

        binding.titleTxt.setText(item.getTitle());
        binding.descriptionTxt.setText(item.getDescription());
        binding.priceTxt.setText("$" + item.getPrice());
        binding.ratingTxt.setText(item.getRating() + " Rating");
        binding.SellerNameTxt.setText(item.getSellerName());

        binding.AddtoCartBtn.setOnClickListener(v -> {
            item.setNumberInCart(numberOrder);
            managentCart.insertItems(item);
        });

        binding.backBtn.setOnClickListener(v -> finish());
        binding.CartBtn.setOnClickListener(v -> startActivity(new Intent(DetailActivity.this, CartActivity.class)));

        Glide.with(this)
                .load(item.getSellerPic())
                .apply(RequestOptions.bitmapTransform(new CenterCrop()))
                .into(binding.picSeller);
        binding.msgToSellerBtn.setOnClickListener(v -> {
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.setData(Uri.parse("sms:" + item.getSellerTell()));
            sendIntent.putExtra("sms_body", "type your message");
            startActivity(sendIntent);
        });
        binding.callToSellerBtn.setOnClickListener(v -> {
            String phone = String.valueOf(item.getSellerTell());
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
            startActivity(intent);
        });
    }
}
