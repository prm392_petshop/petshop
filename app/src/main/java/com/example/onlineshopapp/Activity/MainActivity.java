package com.example.onlineshopapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import com.example.onlineshopapp.Adapter.BestSellerAdapter;
import com.example.onlineshopapp.Adapter.CategoryAdapter;
import com.example.onlineshopapp.Adapter.SliderAdapter;
import com.example.onlineshopapp.Model.CategoryModel;
import com.example.onlineshopapp.Model.ItemsModel;
import com.example.onlineshopapp.Model.SliderModel;
import com.example.onlineshopapp.R;
import com.example.onlineshopapp.ViewModel.MainViewModel;
import com.example.onlineshopapp.databinding.ActivityMainBinding;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class MainActivity extends BaseActivity {
    private MainViewModel viewModel = new MainViewModel();
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("message");

        myRef.setValue("Hello, World!");

        initBanner();
        initCategories();
        initBestSeller();
        bottomNavigation();
    }

    private void bottomNavigation() {
        binding.cartBtn.setOnClickListener(v -> startActivity(new Intent(this, CartActivity.class)));
    }

    private void initBestSeller() {
        binding.progressBarBestSeller.setVisibility(View.VISIBLE);
        viewModel.getBestSeller().observe(this, new Observer<List<ItemsModel>>() {
            @Override
            public void onChanged(List<ItemsModel> itemsModels) {
                binding.viewBestSeller.setLayoutManager(new GridLayoutManager(MainActivity.this, 2));
                binding.viewBestSeller.setAdapter(new BestSellerAdapter(itemsModels));
                binding.progressBarBestSeller.setVisibility(View.GONE);
            }
        });
        viewModel.loadBestSeller();
    }

    private void initCategories() {
        binding.progressBarCategory.setVisibility(View.VISIBLE);
        viewModel.getCategory().observe(this, new Observer<List<CategoryModel>>() {
            @Override
            public void onChanged(List<CategoryModel> categoryModels) {
                binding.viewCategory.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false));
                binding.viewCategory.setAdapter(new CategoryAdapter(categoryModels));
                binding.progressBarCategory.setVisibility(View.GONE);
            }
        });
        viewModel.loadCategory();
    }

    private void initBanner() {
        binding.progressBarBanner.setVisibility(View.VISIBLE);
        viewModel.getBanners().observe(this, new Observer<List<SliderModel>>() {
            @Override
            public void onChanged(List<SliderModel> sliderModels) {
                banners(sliderModels);
                binding.progressBarBanner.setVisibility(View.GONE);
            }
        });
        viewModel.loadBanners();
    }

    private void banners(List<SliderModel> images) {
        binding.viewPagerSlider.setAdapter(new SliderAdapter(images, binding.viewPagerSlider));
        binding.viewPagerSlider.setClipToPadding(false);
        binding.viewPagerSlider.setClipChildren(false);
        binding.viewPagerSlider.setOffscreenPageLimit(3);
        binding.viewPagerSlider.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
        compositePageTransformer.addTransformer(new MarginPageTransformer(40));
        binding.viewPagerSlider.setPageTransformer(compositePageTransformer);
        if (images.size() > 1) {
            binding.dotIndicator.setVisibility(View.VISIBLE);
            binding.dotIndicator.attachTo(binding.viewPagerSlider);
        }
    }
}
