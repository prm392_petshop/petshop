package com.example.onlineshopapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.example.onlineshopapp.R;
import com.example.onlineshopapp.databinding.ViewholderSizeBinding;

import java.util.List;

public class SizeListAdapter extends RecyclerView.Adapter<SizeListAdapter.Viewholder> {

    private List<String> items;
    private int selectedPosition = -1;
    private int lastSelectedPosition = -1;
    private Context context;

    public SizeListAdapter(List<String> items) {
        this.items = items;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private final ViewholderSizeBinding binding;

        public Viewholder(ViewholderSizeBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewholderSizeBinding binding = ViewholderSizeBinding.inflate(inflater, parent, false);
        return new Viewholder(binding);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, @SuppressLint("RecyclerView") int position) {
        holder.binding.sizeTxt.setText(items.get(position));
        holder.binding.getRoot().setOnClickListener(v -> {
        lastSelectedPosition = selectedPosition;
        selectedPosition = position;
        notifyItemChanged(lastSelectedPosition);
        notifyItemChanged(selectedPosition);
    });

        if (selectedPosition == position) {
            holder.binding.sizeLayout.setBackgroundResource(R.drawable.green_bg3);
            holder.binding.sizeTxt.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.binding.sizeLayout.setBackgroundResource(R.drawable.grey_bg);
            holder.binding.sizeTxt.setTextColor(context.getResources().getColor(R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
