package com.example.onlineshopapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;
import com.example.onlineshopapp.Activity.DetailActivity;
import com.example.onlineshopapp.Model.ItemsModel;
import com.example.onlineshopapp.databinding.ViewholderBestSellerBinding;

import java.util.List;

public class BestSellerAdapter extends RecyclerView.Adapter<BestSellerAdapter.Viewholder> {

    private List<ItemsModel> items;
    private Context context;

    public BestSellerAdapter(List<ItemsModel> items) {
        this.items = items;
    }

    public static class Viewholder extends RecyclerView.ViewHolder {
        private final ViewholderBestSellerBinding binding;

        public Viewholder(ViewholderBestSellerBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewholderBestSellerBinding binding = ViewholderBestSellerBinding.inflate(inflater, parent, false);
        return new Viewholder(binding);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, int position) {
        ItemsModel item = items.get(position);

        holder.binding.titleTxt.setText(item.getTitle());
        holder.binding.priceTxt.setText("$" + item.getPrice());
        holder.binding.ratingTxt.setText(String.valueOf(item.getRating()));

        RequestOptions requestOptions = new RequestOptions().transform(new CenterCrop());
        Glide.with(holder.itemView.getContext())
            .load(item.getPicUrl().get(0))
            .apply(requestOptions)
            .into(holder.binding.picBestSeller);

        holder.itemView.setOnClickListener(v -> {
        Intent intent = new Intent(holder.itemView.getContext(), DetailActivity.class);
        intent.putExtra("object", item);
        holder.itemView.getContext().startActivity(intent);
    });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
