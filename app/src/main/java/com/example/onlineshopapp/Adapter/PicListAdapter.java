package com.example.onlineshopapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.example.onlineshopapp.R;
import com.example.onlineshopapp.databinding.ViewholderPicListBinding;

import java.util.List;

public class PicListAdapter extends RecyclerView.Adapter<PicListAdapter.Viewholder> {

    private List<String> items;
    private ImageView picMain;
    private int selectedPosition = -1;
    private int lastSelectedPosition = -1;
    private Context context;

    public PicListAdapter(List<String> items, ImageView picMain) {
        this.items = items;
        this.picMain = picMain;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private final ViewholderPicListBinding binding;

        public Viewholder(ViewholderPicListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewholderPicListBinding binding = ViewholderPicListBinding.inflate(inflater, parent, false);
        return new Viewholder(binding);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, @SuppressLint("RecyclerView") int position) {
        Glide.with(holder.itemView.getContext())
            .load(items.get(position))
            .into(holder.binding.picList);

        holder.binding.getRoot().setOnClickListener(v -> {
        lastSelectedPosition = selectedPosition;
        selectedPosition = position;
        notifyItemChanged(lastSelectedPosition);
        notifyItemChanged(selectedPosition);
        Glide.with(holder.itemView.getContext())
            .load(items.get(position))
            .into(picMain);
    });

        if (selectedPosition == position) {
            holder.binding.picLayout.setBackgroundResource(R.drawable.grey_bg_selected);
        } else {
            holder.binding.picLayout.setBackgroundResource(R.drawable.grey_bg);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
